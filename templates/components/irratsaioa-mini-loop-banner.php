<?php
foreach((array)get_the_category() as $category) { 
    
    $categories[] = $category->name;
    $categoriesMeta = get_field('irratsaioa','category_'.$category->cat_ID,true);
    
    if($categoriesMeta){
        $irratsaioa;
        $irratsaioa['ID'] = $categoriesMeta[0]->ID;
        $irratsaioa['title'] = $categoriesMeta[0]->post_title;
        $irratsaioa['name'] = $categoriesMeta[0]->post_name;
        $irratsaio_info = get_post($categoriesMeta[0]->ID);
        $irratsaio_thumb = get_the_post_thumbnail( $irratsaioa['ID'], 'thumbnail' ); 

        $irratsaio_permalink = get_permalink($categoriesMeta[0]->ID);
    ?>

                <span style="float: right;background: #222;padding: 5px;text-transform: uppercase;"><a href="<?php echo $irratsaio_permalink;?>" style="color:#fff;text-decoration:underline;"><?php echo $categoriesMeta[0]->post_title;?></a></span>

<?php } // if
} // foreach  ?>