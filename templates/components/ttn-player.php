<div id="ttn-player"  ng-app="ttnApp" ng-controller="ttnPlayer" class="{{fullPlayer ? 'full':'small'}}" >

    <div class="container">

        <audio type="audio/ogg; codecs=vorbis" preload="none" id="ogg-stream1" src="https://stream.ttanttakun.org/ttanttakun.ogg"></audio>
        <div class="ttn-player-console" style="height:100px;background-color:#222;border:1px solid #222;">

            <div class="ttn-player-current-item" style="float:left;background-color:#222;">
                <!--<div style="background-image:url({{currentItem.imageSrc}});background-size:cover;background-position:center;height:100%;">

                </div>-->

                <img ng-src="{{currentItem.imageSrc}}"/>
            </div>



            <div class="ttn-player-display">
                <div class="ttn-player-display-wrapper">

                    <div ng-if="currentItem.type==='stream'">
                        <div style="border-bottom: 3px dotted #AAA;">

                            <span ng-hide="now.type==1||now.type==2" style="font-weight:bold;text-transform:uppercase;color:#666;">{{now.izena}}</span>
                            <a ng-show="now.type==1||now.type==2" ng-href="{{currentIrratsaioa.link}}"  style="font-weight:bold;text-transform:uppercase;">{{now.izena}}</a>
                            <small style="color:#666;float:right;" ng-controller="ttnStats" title="{{listeners}} entzule">
                                <span class="glyphicon glyphicon-headphones"></span> {{listeners}}
                            </small>
                            <small style="color:#666;float:right;margin-right:10px;">{{now.hasiera}} - {{now.bukaera}}</small>
                        </div>

                        <div style="line-height:15px;">
                            <small style="color:#777" ng-show="now.type==1 || now.type==5">ZUZENEAN</small>
                            <small style="color:#777" ng-show="now.type==2">ERREPIKAPENA</small>
                            <small title="{{nowPlaying.artist}} - {{nowPlaying.title}}" ng-hide="now.type==1||now.type==2||now.type==5" style="color:#777">
                                <span class="glyphicon glyphicon-user"></span> {{nowPlaying.artist}} <br>
                                <span class="glyphicon glyphicon-music"></span> {{nowPlaying.title.length>40 ? nowPlaying.title.substring(0,35)+'...' : nowPlaying.title}}
                            </small>
                        </div>
                    </div>

                    <div ng-if="currentItem.type==='podcast'">

                        <div>
                            <a ng-href="{{currentItem.postUrl}}" title="{{currentItem.title}}" style="font-weight:bold;text-transform:uppercase;">{{currentItem.title.length>22 ? currentItem.title.substring(0,22)+'...' : currentItem.title}}</a>
                        </div>

                        <div onclick="ttnPlayer.control.seek(event)" id="ttn-player-progress" class="progress" style="background:#555;box-shadow:none;height:10px;border-radius:0;margin-bottom:0;">
                          <div   class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="box-shadow:none;width: {{completed}}%;">
                            <span class="sr-only">{{completed}}%</span>
                          </div>
                        </div>

                        <div style="line-height:30px;">

                            <a style="float:left;color:#555;text-transform:uppercase;font-size:9px;" ng-href="{{currentPodcastIrratsaioa.link}}">{{currentPodcastIrratsaioa.title}}</a>

                            <small style="float:right;color:#666;">{{currentTime | secondsToDateTime | date:'HH:mm:ss'}} / <span ng-if="!currentDuration">?</span><span ng-if="currentDuration">{{currentDuration | secondsToDateTime | date:'HH:mm:ss'}}</span></small>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ttn-player-controls">
                <span ng-show="state.stopped" class="ttn-player-btn ttn-player-play" onclick="ttnPlayer.control.play()">
                    <span class="glyphicon glyphicon-play" style="padding: 0px 5px;"></span>
                </span>
                <span ng-show="state.playing || state.paused" class="ttn-player-btn ttn-player-pause" onclick="ttnPlayer.control.pause()" style="background-color:{{state.paused ? '#666' : 'transparent' }}">
                    <span class="glyphicon glyphicon-pause" style="padding: 0px 4px;"></span>
                </span>
                <span class="ttn-player-btn ttn-player-stop" onclick="ttnPlayer.control.stop()">
                    <span class="glyphicon glyphicon-stop" style="padding: 0px 4px;"></span>
                </span>



                <span class="ttn-player-btn ttn-player-next" onclick="ttnPlayer.control.previous()">
                    <span class="glyphicon glyphicon-step-backward" style="padding: 0px 4px;"></span>
                </span>
                <span class="ttn-player-btn ttn-player-next" onclick="ttnPlayer.control.next()">
                    <span class="glyphicon glyphicon-step-forward" style="padding: 0px 4px;"></span>
                </span>



                <div style="float:right;">
                    <span class="visible-xs ttn-player-btn ttn-player-next" ng-click="toggleFullPlayer();">
                    <span class="glyphicon glyphicon-menu-hamburger" style="padding: 0px 4px;"></span>
                </span>
                    <span class="ttn-player-btn" onclick="ttnPlayer.volume.down()">
                        <span class="glyphicon glyphicon-volume-down" style="padding: 0px 4px;"></span>
                    </span>
                    <span class="ttn-player-btn" style="font-size:14px;padding-top:4px;text-align:center;cursor:default;">
                        {{currentVolume * 100}}
                    </span>
                    <span class="ttn-player-btn" onclick="ttnPlayer.volume.up()">
                        <span class="glyphicon glyphicon-volume-up" style="padding: 0px 4px;"></span>
                    </span>
                </div>

            </div>


        </div>



        <div class="hidden-xs ttn-player-playlist" ng-if="!fullPlayer">
            <div class="ttn-playlist-wrapper" style="position:relative;width:{{53*playlist.items.length}}px">
                <div title="{{item.title}}" ng-repeat="item in playlist.items track by $index" class="ttn-player-playlist-item" style="background-image:url({{item.imageSrc}}); border-color: {{currentItemIndex===$index ? 'red':'white'}}">

                    <div class="ttn-player-playlist-item-overlay">
                        <span class="ttn-overlay-control" style="height:13px;">
                            <a title="{{item.title}}" ng-href="{{item.postUrl}}" style="color:#fff;float:left;"><span class="glyphicon glyphicon-info-sign" ></span></a>

                            <span ng-hide="item.type==='stream'" ng-click="remove($index)" title="Kendu" class="glyphicon glyphicon-remove" style="float:right;"></span>
                        </span>
                        <span class="ttn-overlay-control" style="font-size: 20px;line-height: 10px;padding-left: 15px;padding-top: 0px;text-align: left;">
                            <span ng-click="play($index)" title="entzun" class="glyphicon glyphicon-play" style=""></span>
                        </span>
                        <span class="ttn-overlay-control" style="padding-top:0px;">
                            <span title="Ezkerrera mugitu" class="glyphicon glyphicon-arrow-left" style="float:left;" ng-click="moveItem($index, 'left');"></span>
                            <span title="Eskuinera mugitu" class="glyphicon glyphicon-arrow-right" style="float:right;" ng-click="moveItem($index, 'right');"></span>
                        </span>
                    </div>

                </div>
            </div>
        </div>
        <div class="visible-xs ttn-player-playlist-mobile" ng-if="fullPlayer">
            <div style="height:{{plHeight}}px;overflow-y:scroll;">
            <div ng-repeat="item in playlist.items track by $index" style="margin-bottom:10px; background-color: {{currentItemIndex===$index ? '#777':'transparent'}};padding:10px;">

                            <span ng-click="play($index)" title="entzun" class="glyphicon glyphicon-play" style="margin-right:5px;"></span>
                <div class="ttn-player-playlist-item-controls" style="float:right;">


                            <span title="Ezkerrera mugitu" class="glyphicon glyphicon-chevron-up" style="" ng-click="moveItem($index, 'left');"></span>
                            <span title="Eskuinera mugitu" class="glyphicon glyphicon-chevron-down" style="margin-right:5px;" ng-click="moveItem($index, 'right');"></span>
                            <span ng-hide="item.type==='stream'" ng-click="remove($index)" title="Kendu" class="glyphicon glyphicon-remove" style=""></span>
                    </div>

                <a ng-href="{{item.postUrl}}" style="color:#fff;text-transform:uppercase;">{{item.title}}
                            <a title="{{item.title}}" ng-href="{{item.postUrl}}" style="color:#fff;"><span class="glyphicon glyphicon-info-sign" ></span></a>
            </div>
            </div>

            </div>

        </div>
    </div>

</div>
