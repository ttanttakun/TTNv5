<?php
foreach((array)get_the_category() as $category) { 
    
    $categories[] = $category->name;
    $categoriesMeta = get_field('irratsaioa','category_'.$category->cat_ID,true);
    
    if($categoriesMeta){
        $irratsaioa;
        $irratsaioa['ID'] = $categoriesMeta[0]->ID;
        $irratsaioa['title'] = $categoriesMeta[0]->post_title;
        $irratsaioa['name'] = $categoriesMeta[0]->post_name;
        $irratsaio_info = get_post($categoriesMeta[0]->ID);
        $irratsaio_thumb = get_the_post_thumbnail( $irratsaioa['ID'], 'thumbnail' ); 

        $irratsaio_permalink = get_permalink($categoriesMeta[0]->ID);
    ?>
    
    <div style="background:#222;padding:10px;color:#ddd;">
        <div class="media">
            <a class="pull-left" href="<?php echo $irratsaio_permalink;?> ">
                <?php echo $irratsaio_thumb; ?>
            </a>            
            <div class="media-body">
                <h3 class="media-heading"><a href="<?php echo $irratsaio_permalink;?>" style="color:#fff;text-decoration:underline;"><?php echo $categoriesMeta[0]->post_title;?></a></h3>
                <p><?php echo $categoriesMeta[0]->post_excerpt;?></p>
                <!--<a href="<?php echo $irratsaio_permalink;?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-info-sign"></span> Informazio gehiago</a>-->
            </div>
        </div>
    </div>

<?php } // if
} // foreach  ?>