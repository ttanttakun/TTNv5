<?php
 // Azken audioen scroll bloke horizontala
?>

<div style="overflow-x:scroll;background: #222;margin-bottom:10px;padding:10px;position:relative;" data-snap-ignore="true">
    <div style="height:310px;width:930px;position:relative;">
    <?php
        global $post;

        // $show_posts = of_get_option('slider_options');
        $args = array( 'numberposts' => 12 , 'cat' => 2912, 'category__not_in'=>array(896,4473,3312,9)); // set this to how many posts you want in the carousel
        $myposts = get_posts( $args );
        $post_num = 0;
        foreach( $myposts as $post ) :	setup_postdata($post);

            // $post_thumbnail_id = get_post_thumbnail_id();
            // $featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'wpbs-featured-carousel' );

            $top = 0;
            if($post_num%2!=0){
                $top = 155;
            }
            $left=intval($post_num/2)*155; 
            $post_num++;

        ?>

        <div style="width:150px;height:150px;position:absolute;top:<?php echo $top?>px;left:<?php echo $left?>px;">
            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                <div class="portada-irudia" style="background-image:url('<?php echo catch_that_image(get_the_ID()); ?>');">
                    <span class="portada-irudia-span" style=""><?php the_title(); ?></span>
                    <div class="corner-small"><span class="portada-irudia-data" data="<?php echo the_time('Y-m-d H:i'); ?>"></span></div>

                </div>
            </a>
        </div>

        <?php endforeach; ?>
        <script type="text/javascript">
              ( function($) {
                    // we can now rely on $ within the safety of our "bodyguard" function
                    $(document).ready( function() { 
                    $('.portada-irudia-data').each(function(i){
                        var $this = $(this);
                        $this.text(moment($this.attr('data')).fromNow());
                    });
                    } );
                } ) ( jQuery );
        </script>
        
    </div>
</div>
