<?php $meta = get_post_meta( get_the_ID(), 'enclosure', true );



if( ! empty( $meta ) ):
    $metaArray = explode( "\n", $meta );

    $irratsaioa = 'null';
    foreach((array)get_the_category() as $category) {

        $categories[] = $category->name;
        $categoriesMeta = get_field('irratsaioa','category_'.$category->cat_ID,true);
        if($categoriesMeta){
            $irratsaioa = $categoriesMeta[0]->post_title;
        }
    }
?>
    <script>

        var PlaylistItem = {
            id: "p<?php the_ID(); ?>",
            title: "<?php the_title(); ?>",
            type: "podcast",
            irratsaioa:"<?= $irratsaioa; ?>",
            postUrl: "<?php echo get_permalink();?>",
            audioSrc: "<?php echo trim($metaArray[0]); ?>",
            imageSrc: "<?php echo wp_get_attachment_thumb_url( get_post_thumbnail_id(get_the_ID()) ); ?>"
        };

    </script>


<div class="ttn-podcast-controls"  data-spy="affix" data-offset-top="100" data-offset-bottom="40">
    <div style="background:#222;padding:5px;display:block;width:100%;margin-bottom:0px;">
    <button class="btn btn-default btn-sm" onclick="ttnPlayer.control.play(PlaylistItem, true)">
        <span class="glyphicon glyphicon-play"></span>
        <span class="hidden-xs">ENTZUN</span>
</button>

    <div class="btn-group">
      <button type="button" class="btn btn-default btn-sm" onclick="ttnPlayer.playlist.current.addItem(PlaylistItem)">
          <span class="glyphicon glyphicon-plus"></span>
          <span class="hidden-xs">GEHITU</span>
        </button>
      <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
      </button>
      <ul class="dropdown-menu">
        <li><a href="#" onclick="ttnPlayer.playlist.current.addItem(PlaylistItem, 'start')">Zerrenda hasieran</a></li>
        <li><a href="#" onclick="ttnPlayer.playlist.current.addItem(PlaylistItem, 'end')">Zerrenda amaieran</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="#" onclick="ttnPlayer.playlist.current.addItem(PlaylistItem, 'previous')">Aurreko abestia</a></li>
        <li><a href="#" onclick="ttnPlayer.playlist.current.addItem(PlaylistItem, 'next')">Hurrengo abestia</a></li>
      </ul>
    </div>

    <a href="<?php echo trim($metaArray[0]); ?>" target="_blank" download class="btn btn-default btn-sm">
        <span class="glyphicon glyphicon-download"></span>
        <span class="hidden-xs">DESKARGA</span>
    </a>

        </div>

</div>

    <?php if( $pjax ): ?>        
    <script>
        jQuery('.ttn-podcast-controls').affix({
          offset: {
            top: 100,
            bottom: function () {
              return (this.bottom = jQuery('.footer').outerHeight(true))
            }
          }
        })
    </script>
    <?php endif;?>

<style>
    .ttn-podcast-controls.affix {
        top:30px;
    }
</style>


<?php endif;?>
