<?php while (have_posts()) : the_post(); ?>

  <article <?php post_class(); ?> >

      <?php if( $pjax ): ?>        
          <script>
            ttnTitle.setTitle( "<?php the_title();?>" );
          </script>
      <?php endif;?>

      <?php get_template_part('templates/components/ttn','podcast'); ?>
      <?php get_template_part('templates/entry-date'); ?>
      <?php get_template_part('templates/category-banners'); ?>

            <header style="background:rgba(0,0,0,.08);padding:5px;min-height:76px;">
              <h2 class="entry-title" style="margin:0;"><?php the_title(); ?></h2>
            <div class="entry-excerpt">
              <?php echo wp_trim_words(get_the_content()); ?>
            </div>
            </header>

            <img src="<?= wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(), 'large') ); ?>">

            <?php the_tags( '<div style="padding:5px;background:#222;color:#fff;"><span class="glyphicon glyphicon-tags" style="margin-right:10px;"></span>', ', ', '</div>' ); ?>
      <div class="entry-content" style="background:rgba(0,0,0,.08);padding:5px;overflow:hidden;">
              <?php the_content(); ?>
            </div>


    <footer>
        <?php if ( in_category( 'Audioak' )) {
            get_template_part('templates/components/irratsaioa','loop-banner');
        }?>

      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
