<header class="banner navbar navbar-default navbar-fixed-top navbar-inverse" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
          <?php //bloginfo('name'); ?>
          <img src="/img/ttanttakun_150_iluna_alfa.png" style="height:20px;"/>
        </a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
      endif;
      ?>
    </nav>
  </div>
    <div style="position:absolute;top:5px;right:15px;color:#eee;">

        <a style="color:#eee;margin-left:10px;" href="/" class="hidden-xs"><strong>107.2FM</strong> <strong>107.7FM</strong> TTAN TTAKUN</a>
    </div>

</header>
