<article <?php post_class(); ?>>
    <?php get_template_part('templates/entry-date'); ?>
    <div style="float:left;">
            <a href="<?= get_the_permalink();?>">
                <img width="75" src="<?php echo wp_get_attachment_thumb_url( get_post_thumbnail_id(get_the_ID()) ); ?>" alt="<?= $post->post_title;?>" class="media-object"  style="margin-right:10px;"/>
            </a>
       </div>
  <header>
    <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    <?php //get_template_part('templates/entry-meta'); ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
<hr>
