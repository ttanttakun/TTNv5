<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?> >

      <?php if( $pjax ): ?>        
          <script>
            ttnTitle.setTitle( "<?php the_title();?>" );
          </script>
      <?php endif;?>

      <?php //get_template_part('templates/entry-date'); ?>

            <header style="background:rgba(0,0,0,.08);padding:5px;min-height:76px;">
              <h2 class="entry-title" style="margin:0;"><?php the_title(); ?></h2>
            <div class="entry-excerpt">
              <?php //echo wp_trim_words(get_the_content()); ?>
            <?php $taldeak = get_field('taldeak');
        $zigiluak = get_field('zigilua');

        echo '<b>Taldea(k)</b>: ';
        foreach($taldeak as $taldea){
            echo '<a href="'.get_permalink($taldea->ID).'" title='.$taldea->post_title.'>';
            echo $taldea->post_title;
            echo '</a> ';

        }
        echo '<br>';
         echo '<b>Zigilua(k)</b>: ';
        foreach($zigiluak as $zigilua){
            echo '<a href="'.get_permalink($zigilua->ID).'" title='.$zigilua->post_title.'>';
            echo $zigilua->post_title;
            echo '</a> ';

        }
    $elkarrizketak = get_posts(array(
        //'post_type' => 'post',
        'meta_query' => array(
            array(
                'key' => 'diskak',
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE'
            )
        )
));
    foreach($elkarrizketak as $elkarrizketa){
            echo '<br><a href="'.get_permalink($elkarrizketa->ID).'">Aurkezpen elkarrizketa</a>';
    }

?>

    </div>
            </header>

            <img src="<?= wp_get_attachment_url( get_post_thumbnail_id(get_the_ID(), 'large') ); ?>">

            <?php the_tags( '<div style="padding:5px;background:#222;color:#fff;"><span class="glyphicon glyphicon-tags" style="margin-right:10px;"></span>', ', ', '</div>' ); ?>
      <div class="entry-content" style="background:rgba(0,0,0,.08);padding:5px;overflow:hidden;">
           <?php the_content(); ?><hr>
             <?php the_terms( get_the_ID(), 'etiketa', '', ', ', ' ' );?>

            </div>







    <footer>

      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
  </article>
<?php endwhile; ?>
