<?php 

$postDate = get_the_time('U');
$postEguna =  date_i18n( 'd', $postDate ); 
$postHilabetea =  date_i18n( 'M', $postDate ); 
$postUrtea = date_i18n( 'Y', $postDate ); 

?>


<time class="updated" datetime="<?= get_the_time('c'); ?>" style="background:#222;color:#ddd;float:left;width:70px;display:block;text-align:center;text-transform:uppercase;font-weight:bold;margin-right:5px;margin-bottom:5px;">    
    <span style="display:block;width:100%;"><?= $postHilabetea; ?></span>
    <span style="background:#ccc;display:block;width:100%;color:#222;font-size:25px;"><?= $postEguna; ?></span>
    <span style="display:block;width:100%;"><?= $postUrtea; ?></span>
</time>