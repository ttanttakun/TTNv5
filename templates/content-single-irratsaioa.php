<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
      
    <div style="overflow:hidden;background:#222;padding:5px;color:#ddd;margin-bottom:10px;">
        <?php $irratsaio_thumb = get_the_post_thumbnail( get_the_ID(), 'thumbnail', array( 'class' => 'alignleft','style'=>'margin-bottom:0;' ) ); echo $irratsaio_thumb; ?>
        <header>
          <h1 class="entry-title" style="color:#fff;margin:0;"><?php the_title(); ?></h1>
        </header>
       
        <div class="entry-excerpt">
            <?php echo get_the_excerpt();//wp_trim_words(get_the_content()); ?>
        </div>
      </div>
      <div class="row ttn-irratsaio-ordutegia" style="margin-bottom:10px;min-height:40px;text-align:center;">
          <div class="col-xs-6">
            <h4 class="ttn-title">ZUZENEAN</h4>
              <div class="ttn-irratsaio-ordutegia-zuzenean" style="background:#ccc;"></div>
          </div>
          <div class="col-xs-6">
            <h4 class="ttn-title">ERREPIKAPENA</h4>
            <div class="ttn-irratsaio-ordutegia-errepikapena" style="background:#ccc;"></div>
          </div>
      
      </div>
           
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <?php
            $i_cat = get_field('irratsaio_kategoria',get_the_ID(),true);
            echo do_shortcode('[azkenak cat="'.$i_cat[0]->term_id.'" num="5"]');
        ?>
        
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
    
    <script>
        if(typeof ttnSchedule === 'undefined') {
            setTimeout(function(){
                getSchedule();
            },2000);
        } else {
            getSchedule();
        }
        
        function getSchedule(){
            ttnSchedule.getIrratsaioa('<?= get_the_title(); ?>', function(schedule){
                showSchedule(schedule);
            });
        }
        
        function showSchedule( schedule ){
            console.log(schedule);
            
            var liveOut = '';
            angular.forEach(schedule.live, function(val){
                liveOut += '<div style="display:block;"><b style="text-transform: uppercase;">'+ttnUtils.egunak[val.eguna]+'</b>: ';
                liveOut += '<span>'+val.hasiera+' - '+val.bukaera+'</span></div>';
            })
            var repeatOut = '';
            angular.forEach(schedule.repeat, function(val){
                repeatOut += '<div style="display:block;"><b style="text-transform: uppercase;">'+ttnUtils.egunak[val.eguna]+'</b>: ';
                repeatOut += '<span>'+val.hasiera+' - '+val.bukaera+'</span></div>';
            })
            
            jQuery('.ttn-irratsaio-ordutegia-zuzenean').html(liveOut);
            jQuery('.ttn-irratsaio-ordutegia-errepikapena').html(repeatOut);
        }
        
    </script>
<?php endwhile; ?>
