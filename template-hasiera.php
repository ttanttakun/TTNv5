<?php
/**
* Template Name: TTN Hasiera
*/
?>

<?php if( $pjax ): ?>
          <script>
            ttnTitle.setTitle( "TTAN TTAKUN IRRATIA 107.7FM | ttanttakun.org" );
          </script>
      <?php endif;?>

<?php get_template_part('templates/components/azkenak', 'horizontal'); ?>

<?php


$args = array( 'posts_per_page' => 11 , 'cat' => 5207);
$posts = get_posts( $args );

$first_post = array_shift($posts);

?>

<div class="row">
    <div class="col-md-6 col-lg-7">
        <div style="background:#222;padding:5px; overflow:hidden;min-height:173px;">
            <?php $irudia_tmp = get_post_thumbnail_id($first_post->ID);
	    if($irudia_tmp){
		 $irudia_src = wp_get_attachment_image_src($irudia_tmp,'thumbnail');
         	 $irudia = $irudia_src[0];
	    } else {
		$irudia = '/img/ttanttakun_150_argia.png';
	    }?>
            <img src="<?php echo $irudia; ?>" style="margin-right:5px;margin-bottom:5px;float:left;width:100px;"/>
                <a style="color:#fff;font-weight:bold;text-decoration:underline;" href="<?php echo get_the_permalink($first_post->ID);?>"><?php echo $first_post->post_title;?></a>

            <p style="color:#ddd;"><?php echo wp_trim_words($first_post->post_content, 20);?></p>


        </div>


    </div>
    <div class="col-md-6 col-lg-5">
        <?php ttn_eguerdikoahotsa_output_fn(); ?>
    </div>
</div>
<div class="row">
    <?php foreach($posts as $post): ?>
    <?php $irudia_tmp = get_post_thumbnail_id($post->ID);
	    if($irudia_tmp){
		 $irudia_src = wp_get_attachment_image_src($irudia_tmp,'thumbnail');
         	 $irudia = $irudia_src[0];
	    } else {
		$irudia = '/app/uploads/2013/09/ttanttakun_1072_logo_150_not_alpha.png';
	    }?>
            <div class="col-lg-12" style="margin-bottom:5px;">
        <div style="background:#222;padding:5px; overflow:hidden;">
                <img src="<?php echo $irudia; ?>" style="margin-right:5px;margin-bottom:5px;float:left;width:100px;"/>
                    <a style="color:#fff;font-weight:bold;text-decoration:underline;" href="<?php echo get_the_permalink($post->ID);?>"><?php echo $post->post_title;?></a>

                <p style="color:#ddd;"><?php echo wp_trim_words($post->post_content, 20);?></p>


            </div>
        </div>
    <?php endforeach;?>

</div>
