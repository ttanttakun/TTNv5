<?php 

add_action('init','init_json_feed');
add_filter('query_vars', 'add_query_vars');

add_filter( 'post_limits', 'json_feed_post_limits', 10, 2 );
function json_feed_post_limits( $limits, $wp_query )
{
    if ( $wp_query->is_feed( 'json' ) ) {
        $limits = 'LIMIT 10';
        $limit = (int) get_query_var('limit');
        if(!empty($limit)){
          $limits = 'LIMIT '.$limit;
        }         
    }
    return $limits;
}



function add_query_vars($qvars) {
    $qvars[] = 'callback';
    $qvars[] = 'limit';
    $qvars[] = 'date_format';
    $qvars[] = 'custom';
    return $qvars;
}


function init_json_feed(){
  //global $wp_rewrite;
  add_feed( 'json', 'do_json_feed' );
  //$wp_rewrite->flush_rules();
}



function do_json_feed() {
  global $post;
  $callback = trim(esc_html(get_query_var('callback')));
  $custom = trim(esc_html(get_query_var('custom')));
  $date_format = trim(esc_html(get_query_var('date_format')));
  $charset  = get_bloginfo('charset');
  if ( have_posts() ) {
    $json = array();
    while ( have_posts() ) {
      the_post();
      $id = (int) $post->ID;

      $single = array(
        'id'        => $id ,
        'title'     => get_the_title() ,
        'permalink' => get_permalink(),
        'content'   => get_the_content(),
        'excerpt'   => get_the_excerpt(),
        'date'      => get_the_date(json_feed_date_format($date_format)) ,
        'author'    => get_the_author() ,
        'first_post_image'     => post_irudia(),
        'custom'    => json_feed_custom_vars(get_post_custom())
        );

      // thumbnail
      if (function_exists('has_post_thumbnail') && has_post_thumbnail($id)) {
        $single["thumbnail"] = preg_replace("/^.*['\"](https?:\/\/[^'\"]*)['\"].*/i","$1",get_the_post_thumbnail($id));
        $single["featured"] = preg_replace("/^.*['\"](https?:\/\/[^'\"]*)['\"].*/i","$1",get_the_post_thumbnail($id,'full'));
      }


if ( 'irratsaioa' == get_post_type() ) {
  $single['irratsaio_kategoria'] = get_field('irratsaio_kategoria',$id,true);
  $single['irratsaio_kategoria_url'] =  get_term_link($single['irratsaio_kategoria'][0],null);

}

      // category
      $categories = array();
      
      foreach((array)get_the_category() as $category) { 
        $categories[] = $category->name;
        
          $categoriesMeta = get_field('irratsaioa','category_'.$category->cat_ID,true);
        if($categoriesMeta){
          $irratsaioa;
          $irratsaioa['ID'] = $categoriesMeta[0]->ID;
          $irratsaioa['title'] = $categoriesMeta[0]->post_title;
          $irratsaioa['name'] = $categoriesMeta[0]->post_name;
          // $irratsaioa['all'] = $categoriesMeta[0];
          $single["irratsaioa"] = $irratsaioa;
        }
        


      }
      $single["categories"] = $categories;
      

    $attachments = get_children( array(
      'post_type' => 'any',
      'posts_per_page' => -1,
      'post_parent' => $id,
      'exclude'     => get_post_thumbnail_id()
    ) );
$single["attachments"] = $attachments;

    // if ( $attachments ) {
    //   $attachmentsArray = array();
    //   foreach ( $attachments as $attachment ) {
    //     // $class = "post-attachment mime-" . sanitize_title( $attachment->post_mime_type );
    //     $attachmentsArray[] = wp_get_attachment_link( $attachment->ID, 'thumbnail-size', true );
    //     // echo '<li class="' . $class . ' data-design-thumbnail">' . $thumbimg . '</li>';
    //   }
    //   // $single["attachments"] = $attachmentsArray;
    // }




      // tag
      $posttags = get_the_tags();
      if($posttags) {
        $tags = array();
        foreach($posttags as $tag) { 
          $tags[] = $tag->name; 
        }
        $single["tags"] = $tags;
      } else {
        $single["tags"] = "";
      }

      $json[] = $single;
    }
    $json = json_encode($json);

    nocache_headers();
    if (!empty($callback)) {
      header("Content-Type: application/x-javascript; charset={$charset}");
      echo "{$callback}({$json});";
    } else {
      header("Content-Type: application/json; charset={$charset}");
      echo $json;
    }

  } else {
    header("HTTP/1.0 404 Not Found");
    wp_die("404 Not Found");
  }

}

function json_feed_date_format($date_format)
{
  if (!empty($date_format))
  {
      return $date_format;
  }
  else
  {
      return 'Y-m-d H:i:s';
  }
}  


function json_feed_custom_vars($custom_vars)
{
  if (get_query_var('custom')==''){
    return 'null';  
  } 
  // Delete these following lines if you don't want to expose all your custom fields
  else if(get_query_var('custom')=='all'){
    return $custom_vars;
  }
  // This is OK, do not delete
  else { 
    if(isset($custom_vars[get_query_var('custom')])){
      $custom =  $custom_vars[get_query_var('custom')];
      if(!empty($custom)){
        return $custom;
      } else {
        return '';
      }
    } else {
      return '';
    }
    
  }
}




// function custom_feed_rewrite($wp_rewrite) {
// $feed_rules = array(
// 'json' => 'index.php?feed=json'
// );
// $wp_rewrite->rules = $feed_rules + $wp_rewrite->rules;
// }
// add_filter('generate_rewrite_rules', 'custom_feed_rewrite');
// add_rewrite_rule('^irratsaioak/json','index.php/irratsaioak/?feed=json','top');