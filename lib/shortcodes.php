<?php

//[azken irratsaioak]
function azken_irratsaioak( $atts ){
global $post;
 ob_start();
 ob_end_clean();
extract( shortcode_atts( array(
		'cat' => '3',
		'num' => '4',
	), $atts ) );
$args = array( 'numberposts' => $num, 'offset'=> 0, 'category' => $cat );
$myposts = get_posts( $args );
$tmpPost = $post;
    
echo '<h3 class="ttn-title">AZKEN IRRATSAIOAK</h3>';
    
foreach( $myposts as $post ){
    setup_postdata($post); ?>
   <div style="display:block;overflow:hidden;border-bottom:1px solid #222;;background:#555;padding:5px;color:#eee;">
        <?php get_template_part('templates/entry-date'); ?>
        <div style="float:left;">
            <a href="<?= get_the_permalink();?>">
                <img width="75" src="<?= eman_saioaren_irudia($post->ID);?>" alt="<?= $post->post_title;?>" class="media-object"  style="margin-right:10px;"/>
            </a>
       </div>
       <div>
           <h4 class="media-heading">
                <a style="color:#fff;text-decoration:underline;" href="<?= get_the_permalink();?>">
                   <?= $post->post_title; ?>
                </a>
           </h4>
           <?= wp_trim_words($post->post_content);?>
       </div>
    </div>

<?php }
    
        $post = $tmpPost;
        
        $i_cat_link = get_category_link( $cat );
    
        $catLink = '<a class="btn btn-danger btn-lg btn-block" style="margin-top:10px;margin-bottom:20px;" href="'. $i_cat_link.'">Ikusi '. get_the_title().'(r)en bidalketa guztiak</a>';
        

	return $catLink;
}
add_shortcode( 'azkenak', 'azken_irratsaioak' );

?>