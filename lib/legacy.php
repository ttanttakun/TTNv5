<?php

function post_irudia() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  if(!empty($matches[1])){
    $first_img = $matches [1] [0];
  }
  else{
    $first_img = '/app/uploads/2013/09/ttanttakun_1072_logo_150_not_alpha.png';
  }
  return $first_img;
}


function catch_that_image($post_id) {
  $first_img = eman_saioaren_irudia($post_id);
  return $first_img;
}

function eman_saioaren_irudia($post_id) {
  $irudia= '/app/uploads/2013/09/ttanttakun_1072_logo_150_not_alpha.png';
  $categories = wp_get_post_categories($post_id);
  foreach($categories as $category) {

    $categoriesMeta = get_field('irratsaioa','category_'.$category,true);
    if($categoriesMeta){

      $irratsaio_id = $categoriesMeta[0]->ID;
      $portada_irudia = get_field('portada_irudia', $irratsaio_id,true);

      if($portada_irudia=='lehena') {
          $posta = get_post($post_id);

          $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $posta->post_content, $matches);
          if(!empty($matches[1])){
            $irudia = $matches [1] [0];
          } elseif(empty($matches[1])){
//            $irudia = eman_irratsaioaren_irudia($irratsaio_id);
      	    $irudia_tmp = get_post_thumbnail_id($post_id);
      	    if($irudia_tmp){
      		      $irudia_src = wp_get_attachment_image_src($irudia_tmp,'thumbnail');
               	 $irudia = $irudia_src[0];
      	    } else {
      		      $irudia = eman_irratsaioaren_irudia($irratsaio_id);
      	    }
          }
      } elseif($portada_irudia=='logoa'){
          $post_thumb = get_post_thumbnail_id($irratsaio_id);
          $irudia_src = wp_get_attachment_image_src($post_thumb,'thumbnail');
          $irudia = $irudia_src[0];
      }
    }

  }
  return $irudia;
}


function eman_irratsaioaren_twitter($irratsaio_id){
          $twitter = get_field('twitter',$irratsaio_id,true);
          if($twitter){
            return $twitter;
          } else {
            return 'ttanttakun';
          }
}
