<?php

add_filter( 'json_prepare_post', function ($data, $post, $context) {

  if($post['post_type']==='post'){
  	$enclosure = explode("\n",get_post_meta( $post['ID'], 'enclosure', true ));
  	$data['enclosure'] = $enclosure;
  }
  return $data;
}, 10, 3 );
