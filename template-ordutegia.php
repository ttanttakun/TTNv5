<?php
/**
 * Template Name: TTN ORDUTEGIA Template
 */
?>




<?php if(isset($request_headers['X-Pjax'])): ?>

    <div ng-controller="extOrdutegia" class="panel-group" id="ttn-ordutegia-accordion-ext" role="tablist" aria-multiselectable="true">

         <div class="panel panel-default" ng-repeat="day in schedule">
            <div class="panel-heading ttn-ordutegi-eguna" role="tab" id="{{days[$index]}}-heading">
              <h4 class="panel-title">
                <a style="text-transform:uppercase;" role="button" data-toggle="collapse" data-parent="#ttn-ordutegia-accordion-ext" href="#{{days[$index]}}" aria-expanded="false" aria-controls="{{days[$index]}}">
                  {{days[$index]}}
                </a>
              </h4>
            </div>
            <div id="{{days[$index]}}" class="panel-collapse collapse {{today===$index ? 'in':''}}" role="tabpanel" aria-labelledby="{{days[$index]}}-heading">
              <ul class="list-group">
                <li class="list-group-item ttn-ordutegi-item" ng-repeat="event in schedule[$index]">
                    {{event.hasiera}} <b>{{event.izena}}</b> <span ng-show="event.type===2" class="glyphicon glyphicon-repeat"></span>
                </li>
              </ul>
            </div>
          </div>
        
        
        
        
        
    </div>

   <script>
    
            var app = angular.module('extTtnOrdutegia', [])
              .controller('extOrdutegia', ['$scope','$window', function($scope,$window) {
                  $window.ttnSchedule.getWeek(function(schedule){
                    $scope.schedule = schedule;
                      $scope.days = ttnUtils.egunak;
                      var d = new Date();
                      $scope.today =  d.getDay();
                      $scope.$digest();
                  });
              }]);
        var t = angular.bootstrap(document.getElementById("ttn-ordutegia-accordion-ext"), ['extTtnOrdutegia']);
        
    
            
       jQuery('.collapse').collapse()
    </script> 


<?php else: ?>
<div ng-controller="ordutegia" class="panel-group" id="ttn-ordutegia-accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-default" ng-repeat="day in schedule">
            <div class="panel-heading ttn-ordutegi-eguna" role="tab" id="{{days[$index]}}-heading">
              <h4 class="panel-title">
                <a class="collapsed"  style="text-transform:uppercase;" role="button" data-toggle="collapse" data-parent="#ttn-ordutegia-accordion" href="#{{days[$index]}}" aria-expanded="false" aria-controls="{{days[$index]}}">
                  {{days[$index]}}
                </a>
              </h4>
            </div>
            <div id="{{days[$index]}}" class="panel-collapse collapse {{today===$index ? 'in':''}}" role="tabpanel" aria-labelledby="{{days[$index]}}-heading">
              <ul class="list-group">
                <li class="list-group-item ttn-ordutegi-item" ng-repeat="event in schedule[$index]">
                   {{event.hasiera}} <b>{{event.izena}}</b>
                </li>
              </ul>
            </div>
          </div>

</div>

   <script>
    
        
       function check() {
            if(typeof ttnSchedule === 'undefined') {
                setTimeout(function(){

                    check();
                },1000);
            } else {
                showSchedule();
            }
       }
       
       check();
        
        function showSchedule(){
            angular.module('TtnOrdutegia', [])
              .controller('ordutegia', ['$scope','$window', function($scope,$window) {
                  $window.ttnSchedule.getWeek(function(schedule){
                    $scope.schedule = schedule;
                      $scope.days = ttnUtils.egunak;
                      var d = new Date();
                      $scope.today =  d.getDay();
                      $scope.$digest();
                  });
              }]);
            angular.bootstrap(document.getElementById("ttn-ordutegia-accordion"), ['TtnOrdutegia']);
        }
        
    
            

    </script> 
<?php endif;?>

<style>
.ttn-ordutegi-item {
    background: #555;
    color: #eee;
    border:1px solid #222;
}
    
    .ttn-ordutegi-eguna {
    background: #222!important;
    color: #eee!important;
    border:0!important;
    border-radius:0;
}

</style>





 