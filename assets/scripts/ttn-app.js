(function(){

'use strict';

moment.locale('eu');

var ttnApp = angular.module( 'ttnApp' , [
    'btford.socket-io',
    'ttnApp.services.schedule',
    'ttnApp.services.irratsaioak',
    'ttnApp.services.player'
]);

ttnApp.factory( 'socket' , ['socketFactory', function ( socketFactory ) {
  return socketFactory( {
    ioSocket: io.connect()
  } );
}] );

ttnApp.filter('secondsToDateTime', function() {
    return function(seconds) {
        var d = new Date(0,0,0,0,0,0,0);
        d.setSeconds(seconds);
        return d;
    };
});

ttnApp.factory( 'title' , ['$window', function ( $window ) {

    var title = {
        setPlaying: function(playing){
            if(playing) {
                if( $window.document.title.indexOf('► ') === -1 ) {
                    $window.document.title = '► ' + $window.document.title;
                }
            } else {
                $window.document.title = $window.document.title.replace('► ','');
            }
        },
        setTitle: function(title){
            var play = '';
            if( $window.document.title.indexOf('► ') !== -1 ) {
                play = '► ';
            }
            $window.document.title = play + ' ' + title + ' | ttanttakun.org';
        }
    };

    $window.ttnTitle = title;

    return title;
}] );


ttnApp.controller( 'ttnStats', [ '$scope', 'socket', function( $scope, socket ){
    socket.on('stats', function (stats) {
        $scope.listeners = stats.total_listeners;
    });
}] );

ttnApp.controller( 'ttnPlayer', ['$scope', 'socket', 'Schedule','Player','title','Irratsaioak','$window', function( $scope, socket, Schedule, Player, title, Irratsaioak, $window ){

    Schedule.getNow().then(function(now){
        $scope.now = now;
    });

    Schedule.getTodayIrratsaioak().then(function(todayIrratsaioak){
        $scope.irratsaioak = todayIrratsaioak;
    });

    Schedule.getToday().then(function(today){
        $scope.todayEvents = today;
    });

    $scope.$on('now:updated', function(e,data) {
        Irratsaioak
            .getByName($scope.currentItem.irratsaioa)
            .then(function(irratsaioa){
                $scope.currentPodcastIrratsaioa = irratsaioa;
            });
        $scope.now = data;
    });

    $scope.$on('ttnplayer:state', function(e,data) {
        $scope.state = data;

        if(data.playing) {
            title.setPlaying(true);
        } else {
            title.setPlaying(false);
        }

        $scope.$digest();
    });

    socket.on('metadata', function (metadata) {
        $scope.nowPlaying = metadata;
    });

    socket.on('stats', function (stats) {
        $scope.listeners = stats.total_listeners;
    });

    $scope.playlist = Player.playlist.current;
    $scope.$on('ttnplayer:playlist', function (e, data) {
        $scope.currentItemIndex = Player.playlist.current.position;
        if(data==='external'){
            $scope.$digest();
        }
    });

    $scope.$on('ttnplayer:dataloaded', function (e, data) {
        var duration = Player.player.duration;
        if(duration != Infinity ) {
            $scope.currentDuration = duration;
            $scope.currentTime = 0;
        }
    });

    $scope.$on('ttnplayer:progress', function (e, data) {

        $scope.currentTime = data;


        $scope.completed = Math.floor((data / $scope.currentDuration) * 100);




        $scope.$digest();


    });

    $scope.$on('ttnplayer:currentitem', function(e, data) {
        $scope.currentItem = Player.playlist.current.currentItem();
        $scope.currentItemIndex = Player.playlist.current.position;

        if($scope.currentItem.type==='podcast') {
            Irratsaioak
                .getByName($scope.currentItem.irratsaioa)
                .then(function(irratsaioa){
                    $scope.currentPodcastIrratsaioa = irratsaioa;
                });
        }

        if(data==='external'){
            $scope.$digest();
        }
    });

    $scope.$on('ttnplayer:volume', function(e, data){
        $scope.currentVolume = Player.player.volume;
        //always called from external
        $scope.$digest();
    });

    $scope.currentItem = Player.playlist.current.currentItem();
    $scope.currentItemIndex = Player.playlist.current.position;
    $scope.state = Player.state;
    $scope.currentVolume = Player.player.volume;
    $scope.currentTime = 0;

    $scope.play = function(index){
        Player.control.play(index);
    };

    $scope.moveItem = function(index, direction){
        Player.playlist.current.moveItem(index,direction);
    };

    $scope.remove = function(index) {
        Player.playlist.current.removeItem(index);
    };

    $scope.fullPlayer = false;

    $scope.toggleFullPlayer = function(){
        $scope.fullPlayer = !$scope.fullPlayer;
    };

    $scope.plHeight = $window.innerHeight - 125;

}] );




angular
.module( 'ttnApp.services.player', [] )
.factory('Player', [
    '$rootScope',
    '$window',
    '$timeout',
    function( $rootScope, $window, $timeout ) {

        var PlaylistItem = function(args){

            this.title = args.title;
            this.audioSrc = args.audioSrc;
            this.imageSrc = args.imageSrc;
            this.postUrl = args.postUrl;
            this.description = args.description;
            this.type = args.type;

        };

        var Playlist = function(args){

            if(args && args.title) {
                this.title = args.title;
            } else {
                this.title = "New Playlist";
            }

            this.items = [];

            if(args && args.items) {
                this.items = args.items;
            }

            this.createdAt = new Date();
            this.modifiedAt = new Date();
            this.position = 0;

        };

        Playlist.prototype.length = function(){
            return this.items.length;
        };

        Playlist.prototype.modify = function(action){
            this.modifiedAt = new Date();
            $rootScope.$broadcast('ttnplayer:playlist', action);
        };

        Playlist.prototype.currentItem = function(){
            return  this.items[this.position];
        };

        Playlist.prototype.addItem = function(item, position){

            if(!position) {
                position = 'end';
            }

            switch(position) {
                case 'start':
                    this.items.unshift(item);
                    ttnPlayer.playlist.current.position++;
                    break;
                case 'end':
                    this.items.push(item);
                    break;
                case 'next':
                    this.items.splice(ttnPlayer.playlist.current.position+1, 0, item);
                    break;
                case 'previous':
                    this.items.splice(ttnPlayer.playlist.current.position, 0, item);
                    ttnPlayer.playlist.current.position++;
            }

            this.modify('external');
        };

        Playlist.prototype.moveItem = function(index, direction){
            if(this.length()===1) {return;}
            var to = index;
            if( direction === 'left' || direction === 'up' ) {
                to--;
                if(ttnPlayer.playlist.current.position===to){
                    ttnPlayer.playlist.current.position++;
                } else if(ttnPlayer.playlist.current.position===index){
                    ttnPlayer.playlist.current.position = to;
                }
            } else if( direction === 'right' || direction === 'down' ) {
                to++;

                if(ttnPlayer.playlist.current.position===to){
                    ttnPlayer.playlist.current.position--;
                } else if(ttnPlayer.playlist.current.position===index){
                    ttnPlayer.playlist.current.position = to;
                }
            }

            this.items.splice(to, 0, this.items.splice(index, 1)[0]);

            this.modify('internal');

        };

        Playlist.prototype.removeItem = function(index){

            this.items.splice(index,1);
            if( index===this.position ){
                this.position--;
                ttnPlayer.control.next();
            }
            else if( index<this.position ) {
                this.position--;
            }

            this.modify('internal');
        };



        var ttnPlayer = {
            config : {
                streamItem : {
                    id: 's0',
                    audioSrc: 'https://stream.ttanttakun.org/ttanttakun.ogg',
                    imageSrc: 'https://ttanttakun.org/img/ttanttakun_150_iluna_alfa.png',
                    title: 'Irratia zuzenean (jarioa)',
                    type: 'stream',
                    postUrl: 'http://ttanttakun.org'
                },
                playlists : []
            },
            control: {
                play: function(item, external){
                    if(typeof item != 'undefined' ) {
                        if(typeof item === 'object'){
                            ttnPlayer.playlist.current.addItem(item, 'next');
                            ttnPlayer.control.next(external);
                        } else {
                            ttnPlayer.playlist.current.position = item;
                            item = ttnPlayer.playlist.current.items[item];
                            ttnPlayer.control.load(item, external);
                            ttnPlayer.player.play();
                        }
                    } else if(!ttnPlayer.playlist.current.currentItem){
                        ttnPlayer.control.next();
                    } else {
                        ttnPlayer.player.play();
                    }

                },
                next: function(external){

                    if( ttnPlayer.playlist.current.position !== ( ttnPlayer.playlist.current.length() - 1 ) ) {
                    ttnPlayer.playlist.current.position++;
                    }

                    var item = ttnPlayer.playlist.current.currentItem();
                    ttnPlayer.control.load(item, external);
                    if(ttnPlayer.state.playing || external) {
                        ttnPlayer.control.play();
                    }
                },
                previous: function(){
                    if ( ttnPlayer.playlist.current.position === 0 ) { return; }
                    ttnPlayer.playlist.current.position--;
                    var item = ttnPlayer.playlist.current.currentItem();
                    ttnPlayer.control.load(item);
                    if(ttnPlayer.state.playing) {
                        ttnPlayer.control.play();
                    }
                },
                stop: function(){
                    ttnPlayer.player.pause();
                    $timeout(function(){
                        ttnPlayer.control.load( ttnPlayer.playlist.current.currentItem() );
                        var state = {
                            playing: false,
                            stopped: true,
                            paused: false
                        };
                        ttnPlayer.state = state;
                        $rootScope.$broadcast( 'ttnplayer:state', state );
                    },100);
                },
                pause: function(){
                    if(ttnPlayer.state.paused) {
                        ttnPlayer.player.play();
                    } else {
                        ttnPlayer.player.pause();
                    }
                },
                load: function(item, external){
                    //ttnPlayer.playlist.current.currentItem = item;
                    ttnPlayer.player.src = item.audioSrc;
                    var action = 'internal';
                    if(external) { action='external';}
                    $rootScope.$broadcast('ttnplayer:currentitem', action);
                },
                seek: function(event){

                    var posX = event.clientX;
                    var aud = document.getElementById('ttn-player-progress');
                    var rect = aud.getBoundingClientRect();
                    var currentTime = ( (posX-rect.left) / rect.width ) * ttnPlayer.player.duration;
                    ttnPlayer.player.currentTime = currentTime;

                }

            },

            volume: {
                up: function(){
                    if(ttnPlayer.player.volume<1){
                        ttnPlayer.player.setVolume(
                            Math.round(
                                ( ttnPlayer.player.volume * 100 ) + 10
                            ) / 100
                        );
                        $rootScope.$broadcast( 'ttnplayer:volume', ttnPlayer.player.volume );
                    }
                },
                down: function(){

                    if(ttnPlayer.player.volume>0){
                        ttnPlayer.player.setVolume(
                            Math.round(
                                ( ttnPlayer.player.volume * 100 ) - 10
                            ) / 100
                        );

                        $rootScope.$broadcast( 'ttnplayer:volume', ttnPlayer.player.volume );
                    }
                },

                mute: function(){

                }
            },

            playlist: {
                new: function(){
                    var newPlaylist = new Playlist();
                    ttnPlayer.config.playlists.push(newPlaylist);
                    ttnPlayer.playlist.current = newPlaylist;
                },
                current: undefined
            },

            player: new MediaElement('ogg-stream1',{
                success: function(media, dom){

                    console.log('ttnplayer v0.2');

                    media.addEventListener('loadeddata', function(e) {
                        $rootScope.$broadcast( 'ttnplayer:dataloaded', true );
                    }, false);

                    media.addEventListener('ended', function(e) {
                       var state = {
                            playing: false,
                            stopped: true,
                            paused: false
                        };
                        ttnPlayer.state = state;
                        $rootScope.$broadcast( 'ttnplayer:state', state );

                        if( (ttnPlayer.playlist.current.length()-1) > ttnPlayer.playlist.current.position) {
                            ttnPlayer.control.next();
                        }

                    }, false);

                    media.addEventListener('pause', function(e) {
                        var state = {
                            playing: false,
                            stopped: false,
                            paused: true
                        };
                        ttnPlayer.state = state;
                        $rootScope.$broadcast( 'ttnplayer:state', state );
                    }, false);

                    media.addEventListener('playing', function(e) {
                        var state = {
                            playing: true,
                            stopped: false,
                            paused: false
                        };
                        ttnPlayer.state = state;
                        $rootScope.$broadcast( 'ttnplayer:state', state );

                    }, false);

                    media.addEventListener('timeupdate', function(e) {
                        $rootScope.$broadcast( 'ttnplayer:progress', ttnPlayer.player.currentTime );
                    }, false);
                }
              }),

            state: {
                playing: false,
                stopped: true,
                paused: false
            }
        };

        ttnPlayer.playlist.new();
        ttnPlayer.playlist.current.addItem(ttnPlayer.config.streamItem);
        ttnPlayer.control.load(ttnPlayer.playlist.current.items[0]);
        ttnPlayer.player.setVolume(0.8);

        $window.ttnPlayer = ttnPlayer;

        return ttnPlayer;
    }
]);




angular
.module( 'ttnApp.services.irratsaioak', [] )
.factory( 'Irratsaioak', [
    '$http',
    '$q',
    '$filter',
    function ( $http, $q, $filter ) {

      var irratsaioakPromise = $q.defer();
      var irratsaioak = [];

      $http.get( '/wp-json/irratsaioak' ).success( process );

      function process( response ) {
        irratsaioak = response;
        irratsaioakPromise.resolve( irratsaioak );
      }

      return {
        getAll: function(){
          var deferred = $q.defer();
          irratsaioakPromise.promise.then(function(){
            deferred.resolve(irratsaioak);
          });

          return deferred.promise;
        },
        getByName: function(name){
          var deferred = $q.defer();
          irratsaioakPromise.promise.then(function(){
            deferred.resolve($filter('filter')(irratsaioak, {title: name})[0]);
          });

          return deferred.promise;
        }
      };

}]);


angular
.module('ttnApp.services.schedule', [])
.factory('Schedule', [
    '$http',
    '$interval',
    '$q',
    '$rootScope',
    'Irratsaioak',
    '$filter',
    '$window',
    function ($http, $interval, $q, $rootScope, Irratsaioak, $filter, $window ) {

      var schedulePromise = $q.defer();
      var irratsaioakPromise = $q.defer();
      var allEvents = [];
      var weekEvents = [
        [],[],[],[],[],[],[]
      ];
      var todayEvents = [];
      var nowEvent = {};
      var todayIrratsaioak = {};
      var weekIrratsaioak = {};
      var allIrratsaioak = [];
      var allIrratsaioakPromise = Irratsaioak.getAll();

      $http.get('/wp-json/ordutegia').success(process);

      function process(schedule){
        allIrratsaioakPromise.then(function(allIrratsaioak){
          allIrratsaioak = allIrratsaioak;
          var m = moment();
          var day = m.weekday();
          var promises = [];
          allEvents = schedule.schedule;
          angular.forEach(schedule.schedule, function(val,key){
            weekEvents[val.eguna].push(val);
            if(val.type===1||val.type===2){
              Irratsaioak.getByName(val.izena).then(function(irratsaioa){
                weekIrratsaioak[val.izena] = irratsaioa;
                if(val.eguna===day){
                  todayIrratsaioak[val.izena] = irratsaioa;
                }
              });
            }
            if(val.eguna===day){
              todayEvents.push(val);
            }
          });
          irratsaioakPromise.resolve();
          now();
          schedulePromise.resolve();
          $interval(function(){
            now();
          }, 20000);
        });

      }

      function now(){
        var m = moment();
        angular.forEach(todayEvents, function(val,key){
          var start = moment(val.hasiera, "HH:mm");
          var end   = moment(val.bukaera, "HH:mm");
          if(start.hours()>end.hours()){
            end.add(1,'d');
          }
          var range = moment().range(start, end);

          if(range.contains(m)){
            if(nowEvent!=val){
              $rootScope.$broadcast('now:updated',val);
            }
            nowEvent = val;
          }
        });
      }

      function getScheduleFor(irratsaioa){

var deferred = $q.defer();

        var schedule = {
          live: [],
          repeat:[]
        };
        irratsaioa = irratsaioa.replace(/\s/g, '').toUpperCase();
        angular.forEach(allEvents, function(val, key){
          if(val.izena.replace(/\s/g, '').toUpperCase()===irratsaioa){
            if(val.type===1){
              schedule.live.push(val);
            } else if(val.type===2){
              schedule.repeat.push(val);
            }
          }
        });
        deferred.resolve(schedule);
        return deferred.promise;
      }

      var Schedule = {
        getWeek: function(){
          var deferred = $q.defer();
          schedulePromise.promise.then(function(){
            deferred.resolve(weekEvents);
          });
          return deferred.promise;
        },
        getToday: function(){
          var deferred = $q.defer();
          schedulePromise.promise.then(function(){
            deferred.resolve(todayEvents);
          });
          return deferred.promise;
        },
        getNow: function(){
          var deferred = $q.defer();
          schedulePromise.promise.then(function(){
            deferred.resolve(nowEvent);
          });
          return deferred.promise;
        },
        getTodayIrratsaioak: function(){
          var deferred = $q.defer();
          irratsaioakPromise.promise.then(function(){
            deferred.resolve(todayIrratsaioak);
          });
          return deferred.promise;
        },
        getWeekIrratsaioak: function(){
          var deferred = $q.defer();
          irratsaioakPromise.promise.then(function(){
            deferred.resolve(weekIrratsaioak);
          });
          return deferred.promise;
        },

        getScheduleFor: function(irratsaioa){
          var deferred = $q.defer();
          schedulePromise.promise.then(function(){
            getScheduleFor(irratsaioa).then(function(schedule){
              deferred.resolve(schedule);
            });
          });
          return deferred.promise;
        },
      };

        $window.ttnSchedule = {
            getIrratsaioa: function(irratsaioa, cb){
                Schedule
                    .getScheduleFor(irratsaioa)
                    .then(function(schedule){
                        return cb(schedule);
                    }
                );
            },

            getWeek: function(cb){
                Schedule
                    .getWeek()
                    .then(function(schedule){
                        return cb(schedule);
                    }
                );
            }
        };

        return Schedule;

}]);





})();

var ttnUtils = {
    egunak: [
        'astelehena',
        'asteartea',
        'asteazkena',
        'osteguna',
        'ostirala',
        'larunbata',
        'igandea'
    ]
};
