<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;


$headers = array();
foreach ($_SERVER as $name => $value) {
    if (substr($name, 0, 5) == 'HTTP_') {
        $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
    }
}
$pjax = false;
if(isset($headers['X-Pjax'])){
    $pjax = true;
}


if(!$pjax):

?>

<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        <main class="main" role="main">

<?php else: ?>
            <div id="ttn-ajax-content">
<?php endif;//pjax ?>

            <?php include Wrapper\template_path(); ?>

<?php if(!$pjax): ?>

          </main><!-- /.main -->
        <?php if (Config\display_sidebar()) : ?>
          <aside class="sidebar" role="complementary">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
      <?php get_template_part('templates/components/ttn', 'player'); ?>

      <!-- Piwik -->
      <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(["setDomains", ["*.ttanttakun.org"]]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
          var u="//stats.hackmildegia.net/";
          _paq.push(['setTrackerUrl', u+'piwik.php']);
          _paq.push(['setSiteId', 9]);
          var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
          g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
      </script>
      <noscript><p><img src="//stats.hackmildegia.net/piwik.php?idsite=9" style="border:0;" alt="" /></p></noscript>
      <!-- End Piwik Code -->

  </body>
</html>
<?php else: ?>
    <script>
        (function($) {
            $.each($('a[rel*="'+rllArgs.selector+'"]'), function() {
                var match = $(this).attr('rel').match(new RegExp(rllArgs.selector+'\\[(gallery\\-(?:[\\da-z]{1,4}))\\]', 'ig'));
                if(match !== null) {
                    $(this).attr('data-lightbox-gallery', match[0]);
                }
            });
            $('a[rel*="'+rllArgs.selector+'"]').nivoLightbox();
        })(jQuery);

    </script>
    </div>

<?php endif;//pjax ?>
